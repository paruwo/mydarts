import math
from logging import Logger, getLogger

from rich.console import Console

from mydarts.constants import Constants
from mydarts.dartboard import Dartboard
from mydarts.throw import Throw


class Player:
    """
    A virtual player.

    This one represents a darts match player who can throw darts, counts the throws,
    and keeps basic stats in mind like remaining points or points average.
    """

    name: str
    throws: list[Throw]
    score: int
    console: Console
    log: Logger
    board: Dartboard

    def __init__(
        self,
        name: str,
        high_score: int = 501,
        mode_in: str = Constants.ANY_IN,
        mode_out: str = Constants.DOUBLE_OUT,
    ):
        """Create a player instance."""
        self.console = Console()
        self.log = getLogger()
        self.name = name
        self.throws = []  # of Throws
        self.score = high_score
        self.board = Dartboard(mode_in=mode_in, mode_out=mode_out)

    def undo_last_throw_set(self) -> list:
        """Revert the last throw set, which is needed e.g. if you do not match the finish but threw a too high score."""
        invalid = []

        if len(self.throws) % 3 == 0:
            invalid.append(self.throws.pop())
            invalid.append(self.throws.pop())
            invalid.append(self.throws.pop())
        else:
            while len(self.throws) % 3 != 0:
                invalid.append(self.throws.pop())

        self.log.debug("invalidating last %i throws", len(invalid))
        return invalid

    def throw(self, throw: Throw) -> bool:
        """Reflects a throw, i.e. checks validity of throw, its points etc. and notes it down."""
        return self._throw(value=throw.value, ring=throw.get_ring())

    def _throw(self, value: int, ring: str) -> bool:
        """Reflects a throw, i.e. checks validity of throw, its points etc. and notes it down."""
        if Throw.validate_throw(value, ring):
            if self.points_remaining() == self.score and self.board.mode_in == Constants.DOUBLE_IN and ring != Throw.INTERNAL_DOUBLE:
                self.console.log("missed double-in")
                self.throws.append(Throw("0"))
            else:
                self.throws.append(Throw(str(value) + " " + ring))
        else:
            self.log.warning('invalid number %i thrown, counting "0"', value)
            self.throws.append(Throw("0"))
            return False

        # report after every third dart or in the end
        if len(self.throws) > 0 and len(self.throws) % 3 == 0 and self.points_remaining() > 0:
            self.console.log(f'"{self.name}" threw {self.sum_last_3_throws()}, {self.points_remaining()} remaining')
            self.console.log(f"leg average is {self.average()}")
            return True

        if self.points_remaining() == 0:
            if ring == Throw.INTERNAL_DOUBLE and value in Constants.DOUBLES:
                last_throws = self._get_sum_triplets()[-1]
                self.console.log(f'"{self.name}" finished leg with {last_throws} points')
                if self.is_high_finish():
                    self.console.log("... as high finish!")
                if self.is_shanghai_finish():
                    self.console.log("... wow, a Shanghai finish!")
                if self.is_nine_darter():
                    self.console.log("... unbelievable, a monster nine-darter!")
                if self.is_madhouse_finish():
                    self.console.log("... well done, mad house finish mastered!")
                return True

            self.console.log(f'"{self.name}" missed finish, no double for checkout')
            self.undo_last_throw_set()
            return False

        if self.points_remaining() < 0:
            self.console.log(f'"{self.name}" had too high score with {value} points')
            self.undo_last_throw_set()
            self.console.log(f"{self.points_remaining()} points remaining")
            return False

        # dead simple throw
        return True

    def sum_points(self) -> int:
        """Return the sum of reached points so far."""
        return sum(t.value for t in self.throws)

    def points_remaining(self) -> int:
        """Return the number of remaining points to finish."""
        return self.score - self.sum_points()

    def sum_last_3_throws(self) -> int:
        """Return the sum of the last 3 throws."""
        return sum(t.value for t in self.throws[-3:])

    def has_won(self) -> bool:
        """Check whether the player has won the leg."""
        return self.score == self.sum_points()

    def _get_throw_triplets(self) -> list:
        """Return the latest max. 3 throws."""
        if len(self.throws) == 0:
            return []

        num_triplets = math.ceil(len(self.throws) / 3)
        num_last_throws = len(self.throws) % 3
        if num_last_throws == 0:
            num_last_throws = 3

        triplets = []
        for triplet in range(num_triplets):
            if triplet < num_triplets:
                lower = 3 * triplet
                upper = 3 * (triplet + 1)
                triplets.append(self.throws[lower:upper])
            if triplet == num_triplets:
                triplets.append(self.throws[-num_last_throws:])

        return triplets

    def _get_last_throw_triple(self) -> list:
        """Return the sum of the last 3 throws."""
        triples = self._get_throw_triplets()
        if len(triples) > 0:
            return triples[-1]
        return []

    def _get_sum_triplets(self) -> list:
        """Return the points sum of the latest max. 3 throws."""
        triplets = self._get_throw_triplets()
        sums = []
        for tri in triplets:
            sums.append(sum(t.value for t in tri))

        return sums

    def average(self) -> float:
        """Return the throw average."""
        # average of all past triplets
        sums = self._get_sum_triplets()
        return round(sum(sums) / len(sums), 2)

    def count_ge_100(self) -> int:
        """Return the number of throw with at least 100 points."""
        sums = self._get_sum_triplets()
        return len([x for x in sums if x >= 100])

    def count_ge_140(self) -> int:
        """Return the number of throw with at least 140 points."""
        sums = self._get_sum_triplets()
        return len([x for x in sums if x >= 140])

    def count_180(self) -> int:
        """Return the number of throw with 180 points."""
        sums = self._get_sum_triplets()
        return len([x for x in sums if x == 180])

    def is_nine_darter(self) -> bool:
        """Check whether the leg is a 9-darter."""
        return len(self.throws) == 9

    def is_high_finish(self) -> bool:
        """Check whether a high finish (finish with at least 100 points) is possible."""
        sums = self._get_sum_triplets()
        return sums[-1] >= 100

    def is_madhouse_finish(self) -> bool:
        """Finished leg with D1."""
        return self.has_won() and self.throws[-1].to_official() == "D1"

    def is_shanghai_finish(self) -> bool:
        """Finished leg with all of same segment but different rings, e.g. 120=T20+20+D20."""
        last_throws = self._get_throw_triplets()[-1]
        if len(last_throws) == 3 and self.has_won():
            return all(t.get_segment() == last_throws[0].get_segment() for t in last_throws)
        return False
