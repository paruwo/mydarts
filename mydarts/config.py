"""Configuration tools for MyDarts."""

import logging
from pathlib import Path

import tomllib
from py_singleton import singleton


@singleton
class Config:
    """A wrapper for all configurations from resource/config.toml."""

    root_path = None
    log_level = None
    compress_solution_file = None
    solve_solutions_parallel = None

    def __init__(self):
        """Create instance that holds the configured values from configuration file."""
        self.root_path = Path(__file__).parent.parent

        logging.debug("reading configuration")
        config_file = self.root_path / "resource" / "config.toml"
        with config_file.open("rb") as f:
            config = tomllib.load(f)

        self.log_level = config["general"]["log_level"]
        self.compress_solution_file = bool(config["general"]["compress_solution_file"])
        self.solve_solutions_parallel = bool(
            config["general"]["solve_solutions_parallel"],
        )
