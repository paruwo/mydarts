"""A simple demo console app to show the usage of MyDarts."""

import logging

from rich.console import Console
from rich.logging import RichHandler

from mydarts.config import Config
from mydarts.constants import Constants
from mydarts.player import Player
from mydarts.throw import Throw


class DartGame:
    """The darts game container."""

    game_type = None
    player_1: Player
    player_2: Player

    console: Console

    def __init__(
        self,
        game_type: int,
        mode_in: str = Constants.ANY_IN,
        mode_out: str = Constants.DOUBLE_OUT,
    ):
        """Initialize logger and basic instance attributes."""
        self.console = Console()
        logging.basicConfig()
        logging.getLogger().setLevel(Config().log_level)

        self.game_type = game_type
        self.player_1 = Player(name="Player1", high_score=game_type, mode_in=mode_in, mode_out=mode_out)
        self.player_2 = Player(name="Player2", high_score=game_type, mode_in=mode_in, mode_out=mode_out)

        self.console.log(
            f"The game is {game_type} - {DartGame.mode_to_str(mode=mode_in)} " f"{DartGame.mode_to_str(mode=mode_out)}",
        )

    @staticmethod
    def mode_to_str(mode: str) -> str:
        """Convert a given game mode code like 'do' into human-readable text like 'Double-out'."""
        if mode == Constants.ANY_IN:
            return "Any-In"
        if mode == Constants.DOUBLE_OUT:
            return "Double-out"
        if mode == Constants.DOUBLE_IN:
            return "Double-In"
        if mode == Constants.SINGLE_IN:
            return "Single-In"
        if mode == Constants.TRIPLE_IN:
            return "Triple-In"
        if mode == Constants.TRIPLE_OUT:
            return "Triple-Out"
        if mode == Constants.SINGLE_OUT:
            return "Single-Out"
        if mode == Constants.ANY_OUT:
            return "Any-Out"
        return "<unknown>"

    def play_single_leg(self):
        """Play a single leg."""
        finished = False

        # central game loop
        while not finished:
            for player in [self.player_1, self.player_2]:
                self.console.log()
                self.console.log(
                    f"player '{player.name}' has {player.points_remaining()} points remaining",
                )

                # 3 throws (1-3)
                for num in range(1, 4):
                    points_remaining = player.points_remaining()
                    throws_remaining = 4 - num

                    if player.board.can_finish(
                        remaining_value=points_remaining,
                        num_throws=throws_remaining,
                    ):
                        sol = player.board.preferred_solution(
                            remaining_value=points_remaining,
                            num_throws=throws_remaining,
                        )
                        self.console.log(f"  ... and can finish with {sol}")

                    throw_input = self.console.input(f"{player.name} - {num}. throw: ")
                    throw = Throw(throw_input)
                    valid = player.throw(throw)

                    if not valid:
                        continue
                    if player.has_won():
                        return


if __name__ == "__main__":
    logging.basicConfig(
        level=Config().log_level,
        format="%(message)s",
        datefmt="[%X]",
        handlers=[RichHandler()],
    )

    console = Console()
    console.clear()
    console.rule("[bold red]This is the MyDarts demo program :dart:")
    console.log("Preparing for start...")

    game = DartGame(
        game_type=Constants.GAME_501,
        mode_in=Constants.ANY_IN,
        mode_out=Constants.TRIPLE_OUT,
    )
    game.play_single_leg()
