import re

from mydarts.constants import Constants


class Throw:
    """
    Reflects a darts throw including value and modifiers (double & triple).

    It can be initialized with the official notation (like T20, BULL, 19, D4, ...)
    or an internal notation (like 60 t, 50 d, 19 s, 8 d) which uses the calculated
    values.
    This class supports __str__ (for string serialization), __eq__ and __gt__ (for comparison).
    """

    single = False
    double = False
    triple = False
    value: int

    OFFICIAL_THROW_NOTATION = re.compile(r"^(BULL)|(0)|([DT]?[0-9]{1,3})$", re.IGNORECASE)
    # for 0, the ring modifier does not matter
    INTERNAL_THROW_NOTATION = re.compile(r"^(0|([0-9]{1,3} [sdt]))$")

    # for internal representation
    INTERNAL_SINGLE = "s"
    INTERNAL_DOUBLE = "d"
    INTERNAL_TRIPLE = "t"

    @staticmethod
    def validate_throw(value: int, ring: str) -> bool:
        """Check whether the given value + ring combination do exist."""
        if value == 0:
            return True
        if value in Constants.UNIQUE_VALUES:
            return True
        if ring == Throw.INTERNAL_SINGLE:
            return value in Constants.SECTIONS
        if ring == Throw.INTERNAL_DOUBLE:
            return value in Constants.DOUBLES
        if ring == Throw.INTERNAL_TRIPLE:
            return value in Constants.TRIPLES
        return False

    def __init__(self, throw: str):
        """Initialize a throw instance."""
        # Check structure and eat both, internal and official notation
        # Order matters here, because internal is more restrictive / less ambiguous
        if throw.isdigit() and int(throw) in Constants.UNIQUE_VALUES:
            if int(throw) in Constants.TRIPLES:
                self.parse_internal(throw=str(throw) + " " + Throw.INTERNAL_TRIPLE)
            elif int(throw) in Constants.DOUBLES:
                self.parse_internal(throw=str(throw) + " " + Throw.INTERNAL_DOUBLE)
            elif int(throw) in Constants.SECTIONS:
                self.parse_internal(throw=str(throw) + " " + Throw.INTERNAL_SINGLE)
            elif int(throw) == 0:
                self.parse_internal(throw=str(throw))
            else:
                raise Exception(f'cannot find throw "{throw}" in list of known unified throws')
        elif Throw.INTERNAL_THROW_NOTATION.match(throw):
            self.parse_internal(throw=throw)
        elif Throw.OFFICIAL_THROW_NOTATION.match(throw):
            self.parse_official(throw=throw)
        else:
            raise Exception(f'cannot parse throw "{throw}" at all')

    def __repr__(self):
        """Return a throw as string in official representation."""
        return self.to_official()

    def __str__(self):
        """Return the official representation of a throw."""
        return self.to_official()

    def __eq__(self, other):
        """Compare a throw instance against another based on ring."""
        if isinstance(other, Throw):
            return self.value == other.value and self.single == other.single and self.double == other.double and self.triple == other.triple
        return Exception("cannot compare with other than Throw")

    def __gt__(self, other):
        """Compare to another throw instance based on value."""
        if isinstance(other, Throw):
            return self.value > other.value
        raise Exception("cannot compare with other than Throw")

    def parse_official(self, throw: str):
        """Take a string in official notation (like T10, D15) and extract internal values out of it."""
        # here we know that pattern matches
        if throw[0].lower() == "d":
            self.double = True
            self.value = 2 * int(throw[1:])
        elif throw[0].lower() == "t":
            self.triple = True
            self.value = 3 * int(throw[1:])
        elif throw.lower() == "bull":
            self.double = True
            self.value = 50
        elif throw == "0":
            self.value = 0
        else:
            self.single = True
            self.value = int(throw)

    def parse_internal(self, throw: str):
        """Take a string in internal notation (like 30 t, 30 d) and extracts internal values out of it."""
        # here we know that pattern matches
        self.value = int(throw.split()[0])

        # short circuit
        if self.value == 0:
            return

        modifier = throw.split()[1]
        if modifier == Throw.INTERNAL_SINGLE:
            self.single = True
        elif modifier == Throw.INTERNAL_DOUBLE:
            self.double = True
        elif modifier == Throw.INTERNAL_TRIPLE:
            self.triple = True

    def get_segment(self) -> int:
        """Retrieve the segment, e.g. for D20->20, BULL->25, T13->13, 12->12."""
        if self.double:
            return int(self.value / 2)
        if self.triple:
            return int(self.value / 3)
        return self.value

    def to_official(self) -> str:
        """Convert the throw into official notation, so like T15."""
        multiplier = ""
        printed_value = self.value

        if self.value == 50:
            return "BULL"

        if self.double:
            multiplier = "D"
            printed_value = int(self.value / 2)
        elif self.triple:
            multiplier = "T"
            printed_value = int(self.value / 3)

        return multiplier + str(printed_value)

    def to_internal(self) -> str:
        """Convert the throw into internal notation, so like '45 t'."""
        if self.double:
            modifier = Throw.INTERNAL_DOUBLE
        elif self.triple:
            modifier = Throw.INTERNAL_TRIPLE
        else:
            modifier = Throw.INTERNAL_SINGLE

        return str(self.value) + " " + modifier

    def get_ring(self) -> str:
        """Return the internal ring notation (s, d or t) for this throw."""
        if self.single:
            return self.INTERNAL_SINGLE
        if self.double:
            return self.INTERNAL_DOUBLE
        if self.triple:
            return self.INTERNAL_TRIPLE
        # nothing in case of 0
        return ""
