import datetime
import json
from functools import cache
from logging import Logger, getLogger
from pathlib import Path

import brotli
from joblib import Parallel, delayed
from py_singleton import singleton
from rich.console import Console
from rich.progress import BarColumn, Progress, TimeRemainingColumn

from mydarts.config import Config
from mydarts.constants import Constants
from mydarts.throw import Throw


class Dartboard:
    """This class represents a dart board. It contains all possible throws and has access to checkout solutions."""

    SINGLE_THROWS = [Throw(str(x) + " s") for x in Constants.SECTIONS]
    DOUBLE_THROWS = [Throw(str(x) + " d") for x in Constants.DOUBLES]
    TRIPLE_THROWS = [Throw(str(x) + " t") for x in Constants.TRIPLES]
    ALL_THROWS = SINGLE_THROWS + DOUBLE_THROWS + TRIPLE_THROWS

    BULLS_EYE = Throw("BULL")
    D19 = Throw("D19")
    D17 = Throw("D17")
    D15 = Throw("D15")
    D13 = Throw("D13")
    D11 = Throw("D11")
    D9 = Throw("D9")
    D7 = Throw("D7")
    D5 = Throw("D5")
    D3 = Throw("D3")
    BULL = Throw("25")
    MAD_HOUSE = Throw("D1")

    console: Console
    log: Logger

    mode_in = Constants.ANY_IN
    mode_out = Constants.DOUBLE_OUT

    solutions: dict

    def __init__(self, mode_in: str = Constants.ANY_IN, mode_out: str = Constants.DOUBLE_OUT):
        """
        Create a dartboard instance.

        :param mode_in: not yet implemented
        :param mode_out: player has to finish with any / single / double or triple ring throw, standard is double-out
        """
        self.console = Console()
        self.log = getLogger()
        self.log.debug("initializing dart board instance")

        if mode_in not in Constants.MODES:
            raise Exception("unknown in mode")
        if mode_out not in Constants.MODES:
            raise Exception("unknown out mode")

        self.mode_out = mode_out
        self.mode_in = mode_in

        self.SINGLE_THROWS.sort(reverse=True)
        self.DOUBLE_THROWS.sort(reverse=True)
        self.TRIPLE_THROWS.sort(reverse=True)
        self.ALL_THROWS.sort(reverse=True)

        self.solutions = Solution().solutions

    @staticmethod
    def get_throws_from_mode(mode: str) -> list:
        """Depending on the given game mode, all possible throws are returned."""
        if mode.startswith("d"):
            return Dartboard.DOUBLE_THROWS
        if mode.startswith("s"):
            return Dartboard.SINGLE_THROWS
        if mode.startswith("t"):
            return Dartboard.TRIPLE_THROWS
        return Dartboard.ALL_THROWS

    def check_finishes(self, finish_value: int) -> list:
        """Return all finishing tuples (if existing) for the given potential finishing value."""
        return self.solutions[str(finish_value)]

    def can_finish(self, remaining_value: int, num_throws: int = 3) -> bool:
        """Check whether a finish is possible for the given potential finishing value."""
        if str(remaining_value) not in self.solutions:
            return False
        all_solutions = self.solutions[str(remaining_value)]
        return len([x for x in all_solutions if len(x) <= num_throws]) > 0

    def preferred_solution(self, remaining_value: int, num_throws: int = 3) -> list:
        """
        Return single preferred solution by preference heuristics.

         - prefer short solutions
         - abuse odd final throws (e.g. D11, D5, ...)
         - prefer D20, D18, D16, ... for checkout
         - abuse D1 as well as Bull
         - abuse tiny triples (T1, T2, ... T9)
         - abuse tiny doubles (D1, D2, ... D7)
        """
        if self.mode_out != Constants.DOUBLE_OUT:
            self.log.warning("so far, only DOUBLE-OUT games are fully supported")
            return self.solutions[str(remaining_value)][0]

        if not self.can_finish(remaining_value=remaining_value):
            return []

        preferred_solutions = self.solutions[str(remaining_value)]
        single_checkouts = [x for x in preferred_solutions if len(x) == 1]
        single_checkout_exists = len(single_checkouts) > 0
        double_checkout = [x for x in preferred_solutions if len(x) == 2 and self.MAD_HOUSE not in x]
        double_checkout_exists = len(double_checkout) > 0

        # if there are short solutions, do prefer them
        if double_checkout_exists and not single_checkout_exists:
            preferred_solutions = double_checkout
        elif single_checkout_exists:
            preferred_solutions = single_checkouts

        # The ordering of rules matters
        for checkout in preferred_solutions:
            if len(checkout) > num_throws:
                continue
            # skip bulls if there are more solutions
            if len(preferred_solutions) > 1 and (self.BULLS_EYE in checkout or self.BULL in checkout):
                continue
            # abuse mad house if there are more solutions
            if len(preferred_solutions) > 1 and self.MAD_HOUSE in checkout:
                continue
            # skip awkward odd doubles because if not hit, the fallback score is harder to reach
            if any(
                co in checkout
                for co in [
                    self.D19,
                    self.D17,
                    self.D15,
                    self.D13,
                    self.D11,
                    self.D9,
                    self.D7,
                    self.D5,
                    self.D3,
                ]
            ):
                continue
            # abuse low triples (<T10)
            if len(preferred_solutions) > 1 and any(x.get_ring() == Throw.INTERNAL_TRIPLE and x.value < 30 for x in checkout):
                continue
            # double-out: last must be double, prefer even values for better backup (Tops, D18, D16, ...)
            if self.mode_out == Constants.DOUBLE_OUT and checkout[-1].get_segment() >= 8 and checkout[-1].get_segment() % 2 == 0:
                return checkout

        # in doubt, propose first solution
        return preferred_solutions[0]


@cache
def _gen_all(remaining_value: int, finish_value: int, throw2: Throw, throw3: Throw) -> list[list[Throw]]:
    solutions = []
    for throw1 in (t for t in Dartboard.ALL_THROWS if t.value <= finish_value - throw3.value - throw2.value):
        if throw1.value + remaining_value == finish_value:
            solutions.append([throw1, throw2, throw3])
            continue
    return solutions


@staticmethod
def generate_finishes(finish_value: int, mode_out: str = Constants.DOUBLE_OUT) -> list:
    """Generate all darts solutions for a given checkout value."""
    if finish_value > Constants.HIGHEST_FINISH:
        return []
    if finish_value in Constants.NO_FINISH:
        return []

    solutions = []

    # start with last throw (single/double/triple out)
    for throw3 in (t for t in Dartboard.get_throws_from_mode(mode_out) if t.value <= finish_value):
        if throw3.value == finish_value:
            solutions.append([throw3])
            continue
        for throw2 in (t for t in Dartboard.ALL_THROWS if t.value <= finish_value - throw3.value):
            if throw2.value + throw3.value == finish_value:
                solutions.append([throw2, throw3])
                continue
            # solutions.append(Solution.gen_all()
            for throw1 in (t for t in Dartboard.ALL_THROWS if t.value <= finish_value - throw3.value - throw2.value):
                if throw1.value + throw2.value + throw3.value == finish_value:
                    solutions.append([throw1, throw2, throw3])
                    continue

    return solutions


@singleton
class Solution:
    """
    Container to hold checkout solutions only once for later reuse.

    If the solutions file does not yet exist, it is created. Works as singleton.
    """

    solutions: dict
    console: Console
    log: Logger

    def __init__(self, mode_out: str = Constants.DOUBLE_OUT):
        """Create a solution instance which keeps all checkout solutions."""
        self.console = Console()
        self.log = getLogger()
        self.solutions = {}

        # change finishing file according to game mode, in-mode does not affect finishing solutions
        # read any of compressed or uncompressed
        sol_file_name = "finishings_" + mode_out + ".json"
        sol_file_name_compressed = sol_file_name + ".br"

        sol_file = Config().root_path / "resource" / sol_file_name
        sol_file_compressed = Config().root_path / "resource" / sol_file_name_compressed

        if sol_file_compressed.exists() and sol_file_compressed.is_file():
            self.log.debug("using compressed solutions file")
            self.finishing_file = sol_file_compressed
        elif sol_file.exists() and sol_file.is_file():
            self.log.debug("using uncompressed solutions file")
            self.finishing_file = sol_file
        else:
            if Config().compress_solution_file:
                self.finishing_file = sol_file_compressed
            else:
                self.finishing_file = sol_file

            self.log.info(
                "solutions file %s not existing, creating it now (few seconds)",
                self.finishing_file.name,
            )
            self.generate_finishings_file()

        tmp = Solution.read_finishing_file(finishing_file=self.finishing_file)

        progress = Progress(
            "[progress.description]{task.description}",
            BarColumn(),
            "{task.completed} of {task.total} keys processed",
            TimeRemainingColumn(),
        )

        self.solutions = {}
        with progress:
            task = progress.add_task("loading solutions", total=170)

            # load Throw objects, again naive approach
            for key in tmp:
                solution = []
                # per solution tuple
                for sol in tmp[key]:
                    solution.append([Throw(x) for x in sol])
                progress.update(task, advance=1)
                self.solutions[str(key)] = solution

    @staticmethod
    def read_finishing_file(finishing_file: Path):
        """Extract json content from finishing file."""
        if finishing_file.suffix == ".br":
            with finishing_file.open("rb") as sol_file:
                return json.loads(s=brotli.decompress(sol_file.read()))
        else:
            with finishing_file.open(mode="r") as sol_file:
                return json.load(fp=sol_file)

    @staticmethod
    def verify_finishings_file(finishing_file: Path) -> bool:
        """Verify finishing file against verification file."""
        verification_file = Config().root_path / "resource" / "verification.json"
        if not verification_file.exists():
            getLogger().warning(f"could not read verification file {verification_file}")
            return False
        if not finishing_file.exists():
            getLogger().warning(f"could not read finishing file {finishing_file}")
            return False
        if "_do" not in finishing_file.name:
            getLogger().warning("can only verify double-out finishing file")
            return False

        finishings_data = Solution.read_finishing_file(finishing_file=finishing_file)
        with verification_file.open(mode="r") as ver_file:
            verification_data = json.load(fp=ver_file)

        solution_finishings = {val: len(finishings_data[val]) for val in finishings_data}
        wrong_solutions = {
            k: solution_finishings[k]
            for k in solution_finishings
            if k in verification_data and solution_finishings[k] != verification_data[k]
        }
        return len(wrong_solutions) == 0

    def generate_finishings_file(self):
        """
        Generate a solutions file based on given modes.

        Call the solution generation for all finishes from 1 up to 171 and writes
        the serialized form into a JSON file called 'resources/finshings_%%.json.lz4'.
        The structure is: key - value, where key is the remaining points to finish
        and value is the list of possibles solution tuples.
        """
        start_ts = datetime.datetime.now()

        if Config().solve_solutions_parallel:
            # n_jobs = -2 means "all available -1"
            solution_list = Parallel(n_jobs=-2)(delayed(generate_finishes)(finish_value=x) for x in range(1, Constants.HIGHEST_FINISH + 1))
        else:
            progress = Progress(
                "[progress.description]{task.description}",
                BarColumn(),
                "{task.completed} of {task.total} checkout solutions generated",
                TimeRemainingColumn(),
            )
            with progress:
                task = progress.add_task("solving...", total=Constants.HIGHEST_FINISH)

                solution_list = []
                for finishing_value in range(1, Constants.HIGHEST_FINISH + 1):
                    solution_list.append(generate_finishes(finish_value=finishing_value))
                    progress.update(task, advance=1)

        # copy into dict
        for remaining, solution in enumerate(solution_list, 1):
            self.solutions[str(remaining)] = solution

        self.log.info(
            "generation completed after %f seconds",
            (datetime.datetime.now() - start_ts).total_seconds(),
        )

        # Write json file once for later reuse.
        # As data creation is parallelized (see above), we can spend little more time with a slightly
        # better compression level. Decompression is not affected and stays blazingly fast.
        # We write in compact JSON notation and use Throw in official notation serialization.
        if Config().compress_solution_file:
            with self.finishing_file.open(mode="wb") as file:
                raw = json.dumps(
                    obj=self.solutions,
                    separators=(",", ":"),
                    default=lambda x: x.to_official(),
                    sort_keys=True,
                ).encode("utf-8")
                file.write(brotli.compress(string=raw))
                file.flush()
        else:
            with self.finishing_file.open(mode="w") as file:
                json.dump(
                    obj=self.solutions,
                    fp=file,
                    separators=(",", ":"),
                    default=lambda x: x.to_official(),
                    sort_keys=True,
                )

        if Solution.verify_finishings_file(finishing_file=self.finishing_file):
            self.log.info("solution file verification passed")
        else:
            self.log.error("solution file verification failed, please delete and restart")
