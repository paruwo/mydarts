import collections


class Constants:
    """This is the container for reusable constants."""

    # basic point, doubles and triples
    SECTIONS = [
        20,
        1,
        18,
        4,
        13,
        6,
        10,
        15,
        2,
        17,
        3,
        19,
        7,
        16,
        8,
        11,
        14,
        9,
        12,
        5,
        25,
    ]
    DOUBLES = [2 * single for single in SECTIONS]
    TRIPLES = [3 * single for single in SECTIONS if single <= 20]

    # all possible distinct deduplicated values to throw
    DISTINCT_VALUES = list(set(SECTIONS) | set(DOUBLES) | set(TRIPLES))
    # all possible throw values that are unique, i.e. where is only one way to throw this value
    UNIQUE_VALUES = [0] + list(
        dict(
            filter(
                lambda x: x[1] == 1,
                collections.Counter(SECTIONS + DOUBLES + TRIPLES).items(),
            ),
        ).keys(),
    )

    # special aliases
    BULL = 25
    BULLS_EYE = 50
    TOPS = 40

    # checkout scores without solution, ONLY FOR double-out
    NO_FINISH = [169, 168, 166, 165, 163, 162, 159]
    # highest possible checkout score, ONLY FOR double-out
    HIGHEST_FINISH = 170

    # game scores to reach
    GAME_501 = 501
    GAME_301 = 301

    # end with double (we assume that DOUBLE_OUT is the default mode)
    DOUBLE_OUT = "do"
    # begin with double (default in English pubs)
    DOUBLE_IN = "di"
    TRIPLE_OUT = "to"
    TRIPLE_IN = "ti"
    SINGLE_IN = "si"
    SINGLE_OUT = "so"
    ANY_IN = "ai"
    ANY_OUT = "ao"

    MODES = [
        ANY_IN,
        ANY_OUT,
        DOUBLE_OUT,
        DOUBLE_IN,
        TRIPLE_OUT,
        TRIPLE_IN,
        SINGLE_IN,
        SINGLE_OUT,
    ]
