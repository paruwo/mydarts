import logging

from mydarts.config import Config
from mydarts.constants import Constants
from mydarts.dartboard import Dartboard, Solution


@staticmethod
def generate_finishes(finish_value: int, mode_out: str = Constants.DOUBLE_OUT) -> list:
    """Generate all darts solutions for a given checkout value."""
    if finish_value > Constants.HIGHEST_FINISH:
        return []
    if finish_value in Constants.NO_FINISH:
        return []

    solutions = []

    # start with last throw (single/double/triple out)
    for throw3 in (t for t in Dartboard.get_throws_from_mode(mode_out) if t.value <= finish_value):
        if throw3.value == finish_value:
            solutions.append([throw3])
            continue
        for throw2 in (t for t in Dartboard.ALL_THROWS if t.value <= finish_value - throw3.value):
            if throw2.value + throw3.value == finish_value:
                solutions.append([throw2, throw3])
                continue
            # solutions.append(Solution.gen_all()
            for throw1 in (t for t in Dartboard.ALL_THROWS if t.value <= finish_value - throw3.value - throw2.value):
                if throw1.value + throw2.value + throw3.value == finish_value:
                    solutions.append([throw1, throw2, throw3])
                    continue

    return solutions


if __name__ == "__main__":
    logging.basicConfig()
    logging.getLogger().setLevel(Config().log_level)

    logging.info("You can look up solutions here.")
    solutions = Solution().solutions

    while True:
        key = str(input("Which solution to lookup? "))
        if key in solutions:
            sol1 = [sol for sol in solutions[key] if len(sol) == 1]
            sol2 = [sol for sol in solutions[key] if len(sol) == 2]
            sol3 = [sol for sol in solutions[key] if len(sol) == 3]
            logging.info("single throw %s", sol1)
            logging.info("double throw %s", sol2)
            logging.info("triple throw %s", sol3)
        else:
            logging.info("there is no finish possible for %s", key)
