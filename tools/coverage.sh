#!/bin/sh

cd ..
poetry run poetry run coverage run -m pytest
poetry run coverage html -d test/coverage
browse test/coverage/index.html
