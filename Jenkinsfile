#!/usr/bin/env groovy

// https://www.jenkins.io/doc/book/pipeline/syntax/

pipeline {
    agent {
        label('linux')
    }

    options {
        buildDiscarder logRotator(
            daysToKeepStr: '14',
            numToKeepStr: '10'
        )
    }

    stages {
        stage('Checkout') {
            steps {
                git url: 'https://codeberg.org/paruwo/mydarts.git'
            }
        }

        stage('Setup') {
            steps {
                withPythonEnv('python3.11') {
                    sh '''pip3 install poetry
                          poetry install
                        '''
                }
            }
        }

        stage('Lint') {
            steps {
                withPythonEnv('python3.11') {
                    sh '''poetry run ruff check
                          # poetry run safety check
                       '''
                }
            }
        }

        stage('Test Execution') {
            steps {
                withPythonEnv('python3.11') {
                    sh 'poetry run python -m pytest --junitxml=test/result.xml'
                }
            }
            post {
                always {
                    junit 'test/result.xml'
                }
            }
        }

        stage('Test Coverage') {
            steps {
                withPythonEnv('python3.11') {
                    sh '''cd test
                          poetry run coverage run -m pytest
                          poetry run coverage xml -o coverage.xml
                          poetry run coverage html
                       '''
                    }
            }
            post{
                always{
                    step([$class: 'CoberturaPublisher',
                           autoUpdateHealth: false,
                           autoUpdateStability: false,
                           coberturaReportFile: 'test/coverage.xml',
                           failNoReports: false,
                           failUnhealthy: false,
                           failUnstable: false,
                           maxNumberOfBuilds: 10,
                           onlyStable: false,
                           sourceEncoding: 'ASCII',
                           zoomCoverageChart: false]
                    )
                }
            }
        }

    }
}
