# MyDarts

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

This is MyDarts, a small darts library which offers fundamental darts functionality. It also contains a tiny command line client for demonstration purpose which still needs some love.

## target

* improve Python skills and try out new things
* return something back to community
* promote darts sports

## offerings

* support for OUT games (double/triple/single/any - out)
* thoroughly tested, especially for double-out
* good test coverage
* fast checkout solution generator
* compressed storage of solution files (brotli)
* basic classes with comfortable access to solutions and statistics
* tiny console demo app (with known issues)

## run & test

To run it, you need to have poetry installed, check it out [here](https://python-poetry.org/docs/main/#installing-with-the-official-installer).

### run from Docker

Docker images are neither tested not broadly in use by me - so be warned. However, building the container is straight forward:

```shell
docker builds -t paruwo/darts .
docker run -it --rm --name darts -d paruwo/darts
```

### run from code

Create a virtual environment and install all packages from requirements.txt.
The demo client mydarts/cli.py can be started to play a small 501 leg.

```shell
# install dependencies and mydarts, once only
pyenv virtualenv 3.11 mydarts
pyenv local mydarts
# .. terminal switches to venv
python3 -m pip install poetry==1.8.3
poetry install --only main

# run interactive demo
python3 mydarts/cli.py

# test finishes
python3 mydarts/finisher.py
```

### check code quality & security

```shell
# to run unit tests you need to install dev dependencies, once only
pyenv virtualenv 3.11 mydarts
pyenv local mydarts
# .. terminal switches to venv
python3 -m pip install poetry==1.8.3
python3 -m poetry install

# Linting
ruff check

# Run Unit Tests
pytest
# Run Unit Tests and determine Test Coverage (check test/coverage/index.html)
coverage run -m pytest
```

## credits

* icon taken from [here](https://pixabay.com/vectors/target-arrow-shooting-dart-3823872/)

## TODO

* UX
  * allow to just query endgames (generate all, add cli interface)
  * dockerize
  * allow to enter missed throws to calculate accuracy measures (like double or triple hit rate)
  * support all */* games (hard)
* darts
  * fix apply solution verification only for any-in double-out
* code quality
  * fix and harmonize formatting
  * add unit tests for dartboard.py
* input
  * allow quicker input without ENTER
  * allow input of single unique numbers without modifiers (like 60 instead of T20)
* misc
  * fix long dependency resolution time
  * redirect usual program output to rich console
