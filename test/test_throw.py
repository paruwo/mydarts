from pytest import raises

from mydarts.throw import Throw


def test_validate():
    assert Throw.validate_throw(value=0, ring=Throw.INTERNAL_SINGLE)
    assert Throw.validate_throw(value=1, ring=Throw.INTERNAL_SINGLE)
    assert Throw.validate_throw(value=11, ring=Throw.INTERNAL_SINGLE)
    assert Throw.validate_throw(value=20, ring=Throw.INTERNAL_SINGLE)

    assert Throw.validate_throw(value=2, ring=Throw.INTERNAL_DOUBLE)
    assert Throw.validate_throw(value=50, ring=Throw.INTERNAL_DOUBLE)
    assert Throw.validate_throw(value=34, ring=Throw.INTERNAL_DOUBLE)

    assert Throw.validate_throw(value=3, ring=Throw.INTERNAL_TRIPLE)
    assert Throw.validate_throw(value=15, ring=Throw.INTERNAL_TRIPLE)
    assert Throw.validate_throw(value=33, ring=Throw.INTERNAL_TRIPLE)
    assert Throw.validate_throw(value=60, ring=Throw.INTERNAL_TRIPLE)

    assert not Throw.validate_throw(value=20, ring=Throw.INTERNAL_TRIPLE)


def test_str():
    t = Throw("30 d")
    assert str(t) == "D15"


def test_eq():
    t1 = Throw("D20")
    t2 = Throw("40 d")
    assert t1 == t2


def test_gt():
    t1 = Throw("D9")
    t2 = Throw("20")
    assert t1 < t2

    with raises(Exception) as _:
        assert t1 < "D"  # noqa: E722


def test_validate_throw():
    assert Throw.validate_throw(value=0, ring="s")
    assert Throw.validate_throw(value=1, ring="s")
    assert Throw.validate_throw(value=2, ring="d")
    assert Throw.validate_throw(value=3, ring="t")
    assert Throw.validate_throw(value=33, ring="t")


def test_get_ring():
    t1 = Throw("15")
    assert t1.get_ring() == Throw.INTERNAL_SINGLE
    t2 = Throw("D15")
    assert t2.get_ring() == Throw.INTERNAL_DOUBLE
    t3 = Throw("30 t")
    assert t3.get_ring() == Throw.INTERNAL_TRIPLE


def test_get_segment():
    t1 = Throw("15")
    assert t1.get_segment() == 15
    t2 = Throw("D15")
    assert t2.get_segment() == 15
    t3 = Throw("30 t")
    assert t3.get_segment() == 10


def test_compare():
    t1 = Throw("D15")
    t2 = Throw("T10")
    assert not t1 > t2

    biggest = Throw("D16")
    assert biggest > t1
    assert biggest > t2


def test_parse():
    t = Throw(throw="0 s")
    t.parse_internal("0")
    assert not t.single
    assert not t.double
    assert not t.triple
    assert t.value == 0
    assert t.get_ring() == ""

    t.parse_official("0")
    assert not t.single
    assert not t.double
    assert not t.triple
    assert t.value == 0
    assert t.get_ring() == ""

    t = Throw(throw="T20")
    assert not t.single
    assert not t.double
    assert t.triple
    assert t.value == 60

    t = Throw(throw="60 t")
    assert not t.single
    assert not t.double
    assert t.triple
    assert t.value == 60

    t = Throw(throw="D18")
    assert not t.single
    assert t.double
    assert not t.triple
    assert t.value == 36

    t = Throw(throw="36 d")
    assert not t.single
    assert t.double
    assert not t.triple
    assert t.value == 36

    t = Throw(throw="2")
    assert t.single
    assert not t.double
    assert not t.triple
    assert t.value == 2

    t = Throw(throw="2 s")
    assert t.single
    assert not t.double
    assert not t.triple
    assert t.value == 2


def test_to_official():
    t = Throw(throw="T20")
    assert t.to_official() == "T20"
    t = Throw(throw="60 t")
    assert t.to_official() == "T20"

    t = Throw(throw="D20")
    assert t.to_official() == "D20"
    t = Throw(throw="40 d")
    assert t.to_official() == "D20"

    t = Throw(throw="20")
    assert t.to_official() == "20"
    t = Throw(throw="20 s")
    assert t.to_official() == "20"

    t = Throw(throw="BULL")
    assert t.to_official() == "BULL"


def test_to_internal():
    t = Throw(throw="T20")
    assert t.to_internal() == "60 t"
    t = Throw(throw="60 t")
    assert t.to_internal() == "60 t"

    t = Throw(throw="D20")
    assert t.to_internal() == "40 d"
    t = Throw(throw="40 d")
    assert t.to_internal() == "40 d"

    t = Throw(throw="20")
    assert t.to_internal() == "20 s"
    t = Throw(throw="20 s")
    assert t.to_internal() == "20 s"

    t = Throw(throw="BULL")
    assert t.to_internal() == "50 d"
