import pytest

from mydarts.constants import Constants
from mydarts.player import Player
from mydarts.throw import Throw


def test_simple_wrong_throw():
    p = Player(name="test_remaining", high_score=130)
    with pytest.raises(Exception) as e:
        p.throw(throw=Throw("61"))
        assert e == "cannot find throw 0 in list of known unified throws"


def test_remaining():
    p = Player(name="test_remaining", high_score=130)
    p.throw(throw=Throw("20"))
    p.throw(throw=Throw("D20"))
    p.throw(throw=Throw("T20"))
    assert p.points_remaining() == 10

    p2 = Player(name="test_remaining2", high_score=130)
    p2._throw(value=30, ring=Throw.INTERNAL_DOUBLE)
    p2._throw(value=60, ring=Throw.INTERNAL_TRIPLE)
    p2._throw(value=Constants.TOPS, ring=Throw.INTERNAL_DOUBLE)
    assert p2.points_remaining() == 0

    p3 = Player(name="test_remaining3", high_score=130)
    p3._throw(value=0, ring=Throw.INTERNAL_DOUBLE)
    p3._throw(value=0, ring=Throw.INTERNAL_TRIPLE)
    p3._throw(value=0, ring=Throw.INTERNAL_SINGLE)
    assert p3.points_remaining() == 130


def test_last_3_throws():
    p = Player(name="test_last_3_throws", high_score=130)
    assert p.sum_last_3_throws() == 0

    p._throw(value=1, ring=Throw.INTERNAL_SINGLE)
    assert p.sum_last_3_throws() == 1

    p._throw(value=10, ring=Throw.INTERNAL_SINGLE)
    assert p.sum_last_3_throws() == 11

    p._throw(value=20, ring=Throw.INTERNAL_DOUBLE)
    assert p.sum_last_3_throws() == 31

    p._throw(value=30, ring=Throw.INTERNAL_TRIPLE)
    assert p.sum_last_3_throws() == 60


def test_get_last_throw_triple():
    p = Player(name="test_get_last_throw_triple", high_score=501)

    assert p._get_last_throw_triple() == []

    p._throw(value=1, ring=Throw.INTERNAL_SINGLE)
    p._throw(value=10, ring=Throw.INTERNAL_SINGLE)
    p._throw(value=20, ring=Throw.INTERNAL_DOUBLE)

    assert p._get_last_throw_triple() == [Throw("1"), Throw("10"), Throw("D10")]

    p._throw(value=13, ring=Throw.INTERNAL_SINGLE)

    assert p._get_last_throw_triple() == [Throw("13")]


def test_has_won():
    p = Player(name="test_has_won", high_score=100)

    p._throw(value=40, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=40, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=20, ring=Throw.INTERNAL_DOUBLE)

    assert p.has_won()

    p = Player(name="test_has_won", high_score=100)

    p._throw(value=60, ring=Throw.INTERNAL_TRIPLE)
    p._throw(value=40, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=20, ring=Throw.INTERNAL_DOUBLE)

    assert not p.has_won()

    p = Player(name="test_has_won", high_score=41)

    p._throw(value=0, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=0, ring=Throw.INTERNAL_TRIPLE)
    p._throw(value=40, ring=Throw.INTERNAL_DOUBLE)

    assert not p.has_won()


def test_average():
    p = Player(name="test_average", high_score=501)

    p._throw(value=40, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=40, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=40, ring=Throw.INTERNAL_DOUBLE)

    assert p.average() == 120

    p._throw(value=40, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=40, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=40, ring=Throw.INTERNAL_DOUBLE)

    assert p.average() == 120

    p._throw(value=10, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=10, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=10, ring=Throw.INTERNAL_DOUBLE)

    assert p.average() == 90


def test__get_throw_triplets():
    p = Player(name="test__get_throw_triplets", high_score=501)

    p._throw(value=10, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=20, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=30, ring=Throw.INTERNAL_DOUBLE)

    triplets = p._get_throw_triplets()
    assert triplets == [[Throw("D5"), Throw("D10"), Throw("D15")]]

    p._throw(value=20, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=30, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=40, ring=Throw.INTERNAL_DOUBLE)

    triplets = p._get_throw_triplets()
    assert triplets == [
        [Throw("D5"), Throw("D10"), Throw("D15")],
        [Throw("D10"), Throw("D15"), Throw("D20")],
    ]

    p._throw(value=50, ring=Throw.INTERNAL_DOUBLE)

    triplets = p._get_throw_triplets()
    assert triplets == [[10, 20, 30], [20, 30, 40], [50]]


def test__get_sum_triplets():
    p = Player(name="test__get_sum_triplets", high_score=501)

    p._throw(value=10, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=20, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=30, ring=Throw.INTERNAL_DOUBLE)

    triplets = p._get_sum_triplets()
    assert triplets == [60]

    p._throw(value=20, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=30, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=40, ring=Throw.INTERNAL_DOUBLE)

    triplets = p._get_sum_triplets()
    assert triplets == [60, 90]


def test_count_more_than_100():
    p = Player(name="test_count_more_than_100", high_score=501)

    p._throw(value=40, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=40, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=18, ring=Throw.INTERNAL_DOUBLE)

    assert p.count_ge_100() == 0

    p._throw(value=40, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=40, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=40, ring=Throw.INTERNAL_DOUBLE)

    assert p.count_ge_100() == 1

    p._throw(value=40, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=40, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=20, ring=Throw.INTERNAL_DOUBLE)

    assert p.count_ge_100() == 2


def test_count_ge_140():
    p = Player(name="test_count_ge_140", high_score=501)

    # 139
    p._throw(value=60, ring=Throw.INTERNAL_TRIPLE)
    p._throw(value=40, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=39, ring=Throw.INTERNAL_TRIPLE)

    assert p.count_ge_100() == 1
    assert p.count_ge_140() == 0

    # 140
    p._throw(value=60, ring=Throw.INTERNAL_TRIPLE)
    p._throw(value=40, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=40, ring=Throw.INTERNAL_DOUBLE)

    assert p.count_ge_100() == 2
    assert p.count_ge_140() == 1

    # 150
    p._throw(value=50, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=50, ring=Throw.INTERNAL_DOUBLE)
    p._throw(value=50, ring=Throw.INTERNAL_DOUBLE)

    assert p.count_ge_100() == 3
    assert p.count_ge_140() == 2


def test_count_180():
    p = Player(name="test_count_ge_140", high_score=501)

    # 177
    p._throw(value=57, ring=Throw.INTERNAL_TRIPLE)
    p._throw(value=60, ring=Throw.INTERNAL_TRIPLE)
    p._throw(value=60, ring=Throw.INTERNAL_TRIPLE)

    assert p.count_ge_100() == 1
    assert p.count_ge_140() == 1
    assert p.count_180() == 0

    # 180
    p._throw(value=60, ring=Throw.INTERNAL_TRIPLE)
    p._throw(value=60, ring=Throw.INTERNAL_TRIPLE)
    p._throw(value=60, ring=Throw.INTERNAL_TRIPLE)


def test_high_finish():
    p = Player(name="test_high_finish", high_score=501)

    # 180
    p._throw(value=60, ring=Throw.INTERNAL_TRIPLE)
    p._throw(value=60, ring=Throw.INTERNAL_TRIPLE)
    p._throw(value=60, ring=Throw.INTERNAL_TRIPLE)

    # 180
    p._throw(value=60, ring=Throw.INTERNAL_TRIPLE)
    p._throw(value=60, ring=Throw.INTERNAL_TRIPLE)
    p._throw(value=60, ring=Throw.INTERNAL_TRIPLE)

    # 141 missing...
    p._throw(value=60, ring=Throw.INTERNAL_TRIPLE)
    p._throw(value=57, ring=Throw.INTERNAL_TRIPLE)
    p._throw(value=24, ring=Throw.INTERNAL_DOUBLE)

    assert p.is_high_finish()
    assert p.is_nine_darter()


def test_shanghai_finish():
    p = Player(name="test_high_finish", high_score=501)

    # 180
    p._throw(value=60, ring=Throw.INTERNAL_TRIPLE)
    p._throw(value=60, ring=Throw.INTERNAL_TRIPLE)
    p._throw(value=60, ring=Throw.INTERNAL_TRIPLE)

    # 180
    p._throw(value=60, ring=Throw.INTERNAL_TRIPLE)
    p._throw(value=60, ring=Throw.INTERNAL_TRIPLE)
    p._throw(value=60, ring=Throw.INTERNAL_TRIPLE)

    # 21
    p._throw(value=0, ring=Throw.INTERNAL_SINGLE)
    p._throw(value=1, ring=Throw.INTERNAL_SINGLE)
    p._throw(value=20, ring=Throw.INTERNAL_SINGLE)

    # 120 missing...
    p._throw(value=20, ring=Throw.INTERNAL_SINGLE)
    p._throw(value=60, ring=Throw.INTERNAL_TRIPLE)
    p._throw(value=40, ring=Throw.INTERNAL_DOUBLE)

    assert p.is_high_finish()
    assert p.is_shanghai_finish()


def test_undo_last_throw_set():
    p = Player(name="test_undo_last_throw_set", high_score=150)

    p.throw(Throw("1"))
    p.throw(Throw("2"))
    p.throw(Throw("D3"))

    assert p.undo_last_throw_set() == [6, 2, 1]
    assert len(p.throws) == 0

    p.throw(Throw("1"))
    p.throw(Throw("2"))
    p.throw(Throw("3"))
    p.throw(Throw("4"))

    assert p.undo_last_throw_set() == [4]
    assert len(p.throws) == 3


def test_get_throw_triplets():
    p = Player(name="test_get_throw_triplets", high_score=150)

    assert p._get_throw_triplets() == []

    p.throw(Throw("1"))
    p.throw(Throw("2"))

    assert p._get_throw_triplets() == [[1, 2]]

    p.throw(Throw("D3"))
    p.throw(Throw("4"))

    assert p._get_throw_triplets() == [[1, 2, 6], [4]]

    p.throw(Throw("7"))
    p.throw(Throw("8"))

    assert p._get_throw_triplets() == [[1, 2, 6], [4, 7, 8]]
