import pytest

from mydarts.constants import Constants
from mydarts.dartboard import Dartboard, generate_finishes
from mydarts.throw import Throw


def test_construct():
    with pytest.raises(Exception) as e:
        Dartboard(mode_in="X")
        assert e == "unknown in mode"
    with pytest.raises(Exception) as e:
        Dartboard(mode_out="X")
        assert e == "unknown out mode"


def test_generate_finishes():
    # impossible to reach, too low
    assert generate_finishes(finish_value=1) == []
    # no finish
    assert generate_finishes(finish_value=169) == []
    # too high finish
    assert generate_finishes(finish_value=171) == []

    assert len(generate_finishes(finish_value=2)) == 1
    assert len(generate_finishes(finish_value=3)) == 1
    assert len(generate_finishes(finish_value=100)) == 546
    assert len(generate_finishes(finish_value=144)) == 17

    # only one solution possible
    assert len(generate_finishes(finish_value=2)) == 1
    assert len(generate_finishes(finish_value=3)) == 1
    assert len(generate_finishes(finish_value=153)) == 2  # T19/T20 may be thrown first
    assert len(generate_finishes(finish_value=156)) == 1
    assert len(generate_finishes(finish_value=167)) == 2  # again T19/T20 may be thrown first
    assert len(generate_finishes(finish_value=170)) == 1


def test_can_finish_and_verify_solutions():
    board = Dartboard(mode_out=Constants.DOUBLE_OUT)

    # too high
    assert not board.can_finish(180)
    assert not board.can_finish(172)
    assert not board.can_finish(171)

    # no combination
    assert not board.can_finish(169)
    assert not board.can_finish(168)
    assert not board.can_finish(166)
    assert not board.can_finish(165)
    assert not board.can_finish(163)
    assert not board.can_finish(162)
    assert not board.can_finish(159)

    # one known finish
    assert board.can_finish(170)
    assert len(board.check_finishes(170)) == 1

    # simple
    assert board.can_finish(remaining_value=3, num_throws=3)  # 1,D1
    assert board.can_finish(remaining_value=3, num_throws=2)  # 1,D1
    assert not board.can_finish(remaining_value=3, num_throws=1)

    # mass check, but only the number is checked not the solution itself
    # TODO d.verify_solutions()


def test__get_throws_from_mode():
    assert Dartboard.get_throws_from_mode(mode="si") == Dartboard.SINGLE_THROWS
    assert Dartboard.get_throws_from_mode(mode="do") == Dartboard.DOUBLE_THROWS
    assert Dartboard.get_throws_from_mode(mode="ti") == Dartboard.TRIPLE_THROWS
    assert Dartboard.get_throws_from_mode(mode="i") == Dartboard.ALL_THROWS


# TODO def test_verify_finishing_file():
# TODO   f = Path('.').parent / 'resource' / 'finishing_file_do_json.lz4'
# TODO   assert Solution().verify_finishings_file(finishing_file=f)


def test_no_solution():
    board = Dartboard(mode_out=Constants.DOUBLE_OUT)
    assert board.preferred_solution(remaining_value=171) == []


def test_preferred_solution_double_out():
    board = Dartboard(mode_out=Constants.DOUBLE_OUT)

    # check preferred solutions: taken from https://www.dartn.de/Finish-Liste and slightly adapted
    assert board.preferred_solution(remaining_value=170) == [
        Throw("T20"),
        Throw("T20"),
        Throw("BULL"),
    ]
    assert board.preferred_solution(remaining_value=170) == [
        Throw("60 t"),
        Throw("60 t"),
        Throw("BULL"),
    ]

    assert board.preferred_solution(remaining_value=167) == [
        Throw("T19"),
        Throw("T20"),
        Throw("BULL"),
    ]
    assert board.preferred_solution(remaining_value=164) == [
        Throw("T18"),
        Throw("T20"),
        Throw("BULL"),
    ]
    assert board.preferred_solution(remaining_value=161) == [
        Throw("T17"),
        Throw("T20"),
        Throw("BULL"),
    ]
    assert board.preferred_solution(remaining_value=160) == [
        Throw("T20"),
        Throw("T20"),
        Throw("D20"),
    ]

    assert board.preferred_solution(remaining_value=158) == [
        Throw("T20"),
        Throw("T20"),
        Throw("D19"),
    ]
    assert board.preferred_solution(remaining_value=157) == [
        Throw("T19"),
        Throw("T20"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=156) == [
        Throw("T20"),
        Throw("T20"),
        Throw("D18"),
    ]
    assert board.preferred_solution(remaining_value=155) == [
        Throw("T19"),
        Throw("T20"),
        Throw("D19"),
    ]
    assert board.preferred_solution(remaining_value=154) == [
        Throw("T18"),
        Throw("T20"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=153) == [
        Throw("T19"),
        Throw("T20"),
        Throw("D18"),
    ]
    assert board.preferred_solution(remaining_value=152) == [
        Throw("T20"),
        Throw("T20"),
        Throw("D16"),
    ]
    assert board.preferred_solution(remaining_value=151) == [
        Throw("T17"),
        Throw("T20"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=150) == [
        Throw("T18"),
        Throw("T20"),
        Throw("D18"),
    ]

    assert board.preferred_solution(remaining_value=149) == [
        Throw("T19"),
        Throw("T20"),
        Throw("D16"),
    ]
    assert board.preferred_solution(remaining_value=148) == [
        Throw("T16"),
        Throw("T20"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=147) == [
        Throw("T17"),
        Throw("T20"),
        Throw("D18"),
    ]
    assert board.preferred_solution(remaining_value=146) == [
        Throw("T18"),
        Throw("T20"),
        Throw("D16"),
    ]
    assert board.preferred_solution(remaining_value=145) == [
        Throw("T15"),
        Throw("T20"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=144) == [
        Throw("T16"),
        Throw("T20"),
        Throw("D18"),
    ]
    assert board.preferred_solution(remaining_value=143) == [
        Throw("T17"),
        Throw("T20"),
        Throw("D16"),
    ]
    assert board.preferred_solution(remaining_value=142) == [
        Throw("T14"),
        Throw("T20"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=141) == [
        Throw("T15"),
        Throw("T20"),
        Throw("D18"),
    ]
    assert board.preferred_solution(remaining_value=140) == [
        Throw("T20"),
        Throw("D20"),
        Throw("D20"),
    ]

    assert board.preferred_solution(remaining_value=139) == [
        Throw("T13"),
        Throw("T20"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=138) == [
        Throw("T14"),
        Throw("T20"),
        Throw("D18"),
    ]
    assert board.preferred_solution(remaining_value=137) == [
        Throw("T19"),
        Throw("D20"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=136) == [
        Throw("T20"),
        Throw("D18"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=135) == [
        Throw("T13"),
        Throw("T20"),
        Throw("D18"),
    ]
    assert board.preferred_solution(remaining_value=134) == [
        Throw("T18"),
        Throw("D20"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=133) == [
        Throw("T19"),
        Throw("D18"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=132) == [
        Throw("T20"),
        Throw("D16"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=131) == [
        Throw("T17"),
        Throw("D20"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=130) == [
        Throw("T18"),
        Throw("D18"),
        Throw("D20"),
    ]

    assert board.preferred_solution(remaining_value=129) == [
        Throw("T19"),
        Throw("D16"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=128) == [
        Throw("T16"),
        Throw("D20"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=127) == [
        Throw("T17"),
        Throw("D18"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=126) == [
        Throw("T18"),
        Throw("D16"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=125) == [
        Throw("T15"),
        Throw("D20"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=124) == [
        Throw("T16"),
        Throw("D18"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=123) == [
        Throw("T17"),
        Throw("D16"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=122) == [
        Throw("T14"),
        Throw("D20"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=121) == [
        Throw("T15"),
        Throw("D18"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=120) == [
        Throw("T20"),
        Throw("20"),
        Throw("D20"),
    ]

    assert board.preferred_solution(remaining_value=119) == [
        Throw("T20"),
        Throw("19"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=118) == [
        Throw("T20"),
        Throw("18"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=117) == [
        Throw("T19"),
        Throw("20"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=116) == [
        Throw("T19"),
        Throw("19"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=115) == [
        Throw("T19"),
        Throw("18"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=114) == [
        Throw("T18"),
        Throw("20"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=113) == [
        Throw("T20"),
        Throw("13"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=112) == [
        Throw("T18"),
        Throw("18"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=111) == [
        Throw("T17"),
        Throw("20"),
        Throw("D20"),
    ]
    # possible double-throw solutions starting from here
    assert board.preferred_solution(remaining_value=110) == [
        Throw("T20"),
        Throw("BULL"),
    ]

    assert board.preferred_solution(remaining_value=109) == [
        Throw("T17"),
        Throw("18"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=108) == [
        Throw("T16"),
        Throw("20"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=107) == [
        Throw("T19"),
        Throw("BULL"),
    ]
    assert board.preferred_solution(remaining_value=106) == [
        Throw("T16"),
        Throw("18"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=105) == [
        Throw("T15"),
        Throw("20"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=104) == [
        Throw("T18"),
        Throw("BULL"),
    ]
    assert board.preferred_solution(remaining_value=103) == [
        Throw("T15"),
        Throw("18"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=102) == [
        Throw("T14"),
        Throw("20"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=101) == [
        Throw("T17"),
        Throw("BULL"),
    ]
    assert board.preferred_solution(remaining_value=100) == [Throw("T20"), Throw("D20")]

    assert board.preferred_solution(remaining_value=99) == [
        Throw("T13"),
        Throw("20"),
        Throw("D20"),
    ]
    assert board.preferred_solution(remaining_value=98) == [Throw("T20"), Throw("D19")]
    assert board.preferred_solution(remaining_value=97) == [Throw("T19"), Throw("D20")]
    assert board.preferred_solution(remaining_value=96) == [Throw("T20"), Throw("D18")]
    assert board.preferred_solution(remaining_value=95) == [Throw("T19"), Throw("D19")]
    assert board.preferred_solution(remaining_value=94) == [Throw("T18"), Throw("D20")]
    assert board.preferred_solution(remaining_value=93) == [Throw("T19"), Throw("D18")]
    assert board.preferred_solution(remaining_value=92) == [Throw("T20"), Throw("D16")]
    assert board.preferred_solution(remaining_value=91) == [Throw("T17"), Throw("D20")]
    assert board.preferred_solution(remaining_value=90) == [Throw("T18"), Throw("D18")]

    assert board.preferred_solution(remaining_value=89) == [Throw("T19"), Throw("D16")]
    assert board.preferred_solution(remaining_value=88) == [Throw("T16"), Throw("D20")]
    assert board.preferred_solution(remaining_value=87) == [Throw("T17"), Throw("D18")]
    assert board.preferred_solution(remaining_value=86) == [Throw("T18"), Throw("D16")]
    assert board.preferred_solution(remaining_value=85) == [Throw("T15"), Throw("D20")]
    assert board.preferred_solution(remaining_value=84) == [Throw("T16"), Throw("D18")]
    assert board.preferred_solution(remaining_value=83) == [Throw("T17"), Throw("D16")]
    assert board.preferred_solution(remaining_value=82) == [Throw("T14"), Throw("D20")]
    assert board.preferred_solution(remaining_value=81) == [Throw("T15"), Throw("D18")]
    assert board.preferred_solution(remaining_value=80) == [Throw("D20"), Throw("D20")]

    assert board.preferred_solution(remaining_value=79) == [Throw("T13"), Throw("D20")]
    assert board.preferred_solution(remaining_value=78) == [Throw("T14"), Throw("D18")]
    assert board.preferred_solution(remaining_value=77) == [Throw("T19"), Throw("D10")]
    assert board.preferred_solution(remaining_value=76) == [Throw("D18"), Throw("D20")]
    assert board.preferred_solution(remaining_value=75) == [Throw("T13"), Throw("D18")]
    assert board.preferred_solution(remaining_value=74) == [Throw("T18"), Throw("D10")]
    assert board.preferred_solution(remaining_value=73) == [Throw("T11"), Throw("D20")]
    assert board.preferred_solution(remaining_value=72) == [Throw("D16"), Throw("D20")]
    assert board.preferred_solution(remaining_value=71) == [Throw("T17"), Throw("D10")]
    assert board.preferred_solution(remaining_value=70) == [Throw("T10"), Throw("D20")]

    assert board.preferred_solution(remaining_value=69) == [Throw("T11"), Throw("D18")]
    assert board.preferred_solution(remaining_value=68) == [Throw("D14"), Throw("D20")]
    assert board.preferred_solution(remaining_value=67) == [Throw("T17"), Throw("D8")]
    assert board.preferred_solution(remaining_value=66) == [Throw("T10"), Throw("D18")]
    assert board.preferred_solution(remaining_value=65) == [Throw("T15"), Throw("D10")]
    assert board.preferred_solution(remaining_value=64) == [Throw("D12"), Throw("D20")]
    assert board.preferred_solution(remaining_value=63) == [Throw("T13"), Throw("D12")]
    assert board.preferred_solution(remaining_value=62) == [Throw("T14"), Throw("D10")]
    assert board.preferred_solution(remaining_value=61) == [Throw("T15"), Throw("D8")]
    assert board.preferred_solution(remaining_value=61) == [
        Throw("45 t"),
        Throw("16 d"),
    ]  # test internal
    assert board.preferred_solution(remaining_value=60) == [Throw("20"), Throw("D20")]

    assert board.preferred_solution(remaining_value=59) == [Throw("19"), Throw("D20")]
    assert board.preferred_solution(remaining_value=58) == [Throw("18"), Throw("D20")]
    assert board.preferred_solution(remaining_value=57) == [Throw("17"), Throw("D20")]
    assert board.preferred_solution(remaining_value=56) == [Throw("16"), Throw("D20")]
    assert board.preferred_solution(remaining_value=55) == [Throw("15"), Throw("D20")]
    assert board.preferred_solution(remaining_value=54) == [Throw("14"), Throw("D20")]
    assert board.preferred_solution(remaining_value=53) == [Throw("13"), Throw("D20")]
    assert board.preferred_solution(remaining_value=52) == [Throw("12"), Throw("D20")]
    assert board.preferred_solution(remaining_value=51) == [Throw("11"), Throw("D20")]
    # possible single-throw solutions starting from here
    assert board.preferred_solution(remaining_value=50) == [Throw("BULL")]

    assert board.preferred_solution(remaining_value=49) == [Throw("9"), Throw("D20")]
    assert board.preferred_solution(remaining_value=48) == [Throw("8"), Throw("D20")]
    assert board.preferred_solution(remaining_value=47) == [Throw("7"), Throw("D20")]
    assert board.preferred_solution(remaining_value=46) == [Throw("6"), Throw("D20")]
    assert board.preferred_solution(remaining_value=45) == [Throw("5"), Throw("D20")]
    assert board.preferred_solution(remaining_value=44) == [Throw("4"), Throw("D20")]
    assert board.preferred_solution(remaining_value=43) == [Throw("3"), Throw("D20")]
    assert board.preferred_solution(remaining_value=42) == [Throw("2"), Throw("D20")]
    assert board.preferred_solution(remaining_value=41) == [Throw("1"), Throw("D20")]
    assert board.preferred_solution(remaining_value=40) == [Throw("D20")]

    assert board.preferred_solution(remaining_value=39) == [Throw("3"), Throw("D18")]
    assert board.preferred_solution(remaining_value=38) == [Throw("D19")]
    assert board.preferred_solution(remaining_value=37) == [Throw("1"), Throw("D18")]
    assert board.preferred_solution(remaining_value=36) == [Throw("D18")]
    assert board.preferred_solution(remaining_value=35) == [Throw("15"), Throw("D10")]
    assert board.preferred_solution(remaining_value=34) == [Throw("D17")]
    assert board.preferred_solution(remaining_value=33) == [Throw("13"), Throw("D10")]
    assert board.preferred_solution(remaining_value=32) == [Throw("D16")]
    assert board.preferred_solution(remaining_value=31) == [Throw("11"), Throw("D10")]
    assert board.preferred_solution(remaining_value=31) == [
        Throw("11"),
        Throw("20 d"),
    ]  # test internal
    assert board.preferred_solution(remaining_value=30) == [Throw("D15")]

    assert board.preferred_solution(remaining_value=29) == [Throw("9"), Throw("D10")]
    assert board.preferred_solution(remaining_value=28) == [Throw("D14")]
    assert board.preferred_solution(remaining_value=27) == [Throw("7"), Throw("D10")]
    assert board.preferred_solution(remaining_value=26) == [Throw("D13")]
    assert board.preferred_solution(remaining_value=25) == [Throw("5"), Throw("D10")]
    assert board.preferred_solution(remaining_value=24) == [Throw("D12")]
    assert board.preferred_solution(remaining_value=23) == [Throw("3"), Throw("D10")]
    assert board.preferred_solution(remaining_value=22) == [Throw("D11")]
    assert board.preferred_solution(remaining_value=21) == [Throw("1"), Throw("D10")]
    assert board.preferred_solution(remaining_value=20) == [Throw("D10")]

    assert board.preferred_solution(remaining_value=19) == [Throw("3"), Throw("D8")]
    assert board.preferred_solution(remaining_value=18) == [Throw("D9")]
    assert board.preferred_solution(remaining_value=17) == [Throw("1"), Throw("D8")]
    assert board.preferred_solution(remaining_value=16) == [Throw("D8")]
    assert board.preferred_solution(remaining_value=15) == [Throw("7"), Throw("D4")]
    assert board.preferred_solution(remaining_value=14) == [Throw("D7")]
    assert board.preferred_solution(remaining_value=13) == [Throw("5"), Throw("D4")]
    assert board.preferred_solution(remaining_value=12) == [Throw("D6")]
    assert board.preferred_solution(remaining_value=11) == [Throw("3"), Throw("D4")]
    assert board.preferred_solution(remaining_value=10) == [Throw("D5")]

    assert board.preferred_solution(remaining_value=9) == [Throw("1"), Throw("D4")]
    assert board.preferred_solution(remaining_value=8) == [Throw("D4")]
    assert board.preferred_solution(remaining_value=7) == [Throw("3"), Throw("D2")]
    assert board.preferred_solution(remaining_value=6) == [Throw("D3")]
    assert board.preferred_solution(remaining_value=5) == [Throw("1"), Throw("D2")]
    assert board.preferred_solution(remaining_value=4) == [Throw("D2")]
    assert board.preferred_solution(remaining_value=4) == [Throw("4 d")]
    assert board.preferred_solution(remaining_value=3) == [Throw("1"), Throw("D1")]
    assert board.preferred_solution(remaining_value=2) == [Throw("D1")]
    # (can happen in single-out game)
    assert board.preferred_solution(remaining_value=1) == []
